---
marp: true
size: 4:3
paginate: true
class: invert
---

# INGG1002 - PNS
## **P**rogrammering, **N**umerikk og **S**ikkerhet

<!-- -->
---

<!-- -->
---

![Civilization cover](images/civilization-cover.jpg)

<!-- -->
---

![Civilization gameplay](images/civilization-gameplay.jpg)

<!-- -->
---

![Civilization cover](images/civilization-cover.jpg)

<!-- -->
---

## Tall i datasystemer

<!-- -->
---

![Bike lock](images/bike-lock.jpg)

<!-- -->
---

![Nuclear Gandhi](images/nuclear-gandhi.jpg)
<!-- -->
---

![Om meg](images/om-meg.png)

<!-- 
- navn, alder, tittel
- utdanning: ...
-->
---

![Spider robot](images/spider-robot.jpg)

<!-- 
- BSc dataingeniør
- Oppgave: KM, edderkopp-robot
-->
---

![Hamming distance](images/hd-flips-1.png)

<!-- 
- MSc datavitenskap
- Oppgave: Kryptologi, kryptanalyse
- Differential analysis using Hamming distance
-->
---

![Context Aggregation Network (CAN)](images/can-training.png)

<!-- 
- MSc datavitenskap
- Oppgave: Kryptologi, kryptanalyse
- Differential analysis using a Context Aggregation Network
-->
---

<!-- Hvordan få hjelp?
- Spør studasser eller meg
- Spør på Discord ...
-->
---

![QR-kode til Discord-gruppen](images/discord-qr.png)

<!-- Hvordan få hjelp?
- Scan Discord-koden for å bli med
- Søk på nettet
- Bok, hvis du har det
-->
---

<!-- BB -> JL -> Ny fil -->
