# Enkel test. Mer om dette om to uker.

if 3 < 4:
    print("3 er mindre enn 4")

if 3 == 4:
    print("3 er lik 4")


# Litt mer interessant variant, med bruk av variabler.

x = 3
y = 4

if x < y:
    print("x er mindre enn y")

if x > y:
    print("x er større enn y")

if x == y:
    print("x er lik y")
