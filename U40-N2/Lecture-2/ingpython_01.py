#
# Vi integrerer litt
#

import numpy as np

# funksjonen vi skal integrere
def f(x):
    # return x
    return np.exp(-x**2)

#
# Trapesmetoden
#
h = 0.1    # steglengde
a = -10.0  # nedre grense
b = 10.0   # øvre grense

xverdier = np.arange(a,b + h,h)  # kan godt bruke linspace her....
fverdier = f(xverdier)

# skal lage vektoren trapeskoeff [1,2,2,2,2,1]
lengde = len(xverdier)
trapesKoeff = 2.0*np.ones(lengde)
trapesKoeff[0] = 1.0
trapesKoeff[-1] = 1.0

# Bedre utregning kom fra fra salen, men samme svar
trapesKoeff2 = np.ones(lengde)
# trapesKoeff2[start : stopp før : steglengde]
trapesKoeff2[1:-1] = 2

integralet = (trapesKoeff @ fverdier)  * (h/2.0)
print("integralet er ", integralet)


#
# Simpsons metode
#
h = 0.1
a = 0.0
b = 1.0
xverdier = np.arange(0.0, 1.0 + h, h)
fverdier = f(xverdier)
lengde = len(xverdier)   # Veldig viktig at lengde er oddetall

# Lager vektoren [1,4,2,4,2,.....,2,4,1]
simpsonkoeff = np.ones(lengde)
simpsonkoeff[1:-1:2] = 4
simpsonkoeff[2:-1:2] = 2

integralet = (simpsonkoeff @ fverdier) * (h / 3.0)
print("integralet er ", integralet)

#
# Andre integrasjonsmetoder: er samme utregning, men endrer på koeffisientene
# endrer på xverdier.
#

