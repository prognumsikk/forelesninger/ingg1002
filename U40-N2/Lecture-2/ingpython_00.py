
import numpy as np

# x er et heltall
# x er et flyttall
# x er hva som helst som gjør at x**2 * np.sin(x) + np.cos(x) gir mening
# x kan være np.array([1.0,2.0])
def f(x):
    return x**2 * np.sin(x) + np.cos(x)

print("Funksjonsverdien i x = 3.0 er lik", f(3.0))

v = np.array([1.0,2.0,3.0])
print("Hva ligger i v?", v)
print("Hva liger i f(v)?",f(v))

# Vi deriverer ved "håndregning"
# Vi bruker denne kun for å sammenligne med den numeriske utregningen
def eksaktdfdx(x):
    return x**2 * np.cos(x) + 2.0 * x * np.sin(x) - np.sin(x)

# Hva er x her?
# x er hva som helst som gjør at det under gir mening
def dfdx(x):
    h = 0.01
    deltay = f(x + h) - f(x)
    deltax = h
    return deltay / deltax

print("f(4.0)=",f(4.0))
print("numerisk verdi df/dx(4.0)=",dfdx(4.0))
print("eksakt verdi df/dx(4.0)=",eksaktdfdx(4.0))

v = np.array([1.0,2.0,3.0])
print("f(v)=", f(v))
print("dfdx(v)=",dfdx(v))
print("eksakt dfdx(v)=",eksaktdfdx(v))


#
# Litt eksempler av plotting (vi er ikke så gode på det....)
#
import matplotlib.pyplot as plt

def g(x):
    return np.sin(x)**3 + 1/x**4

def dgdx(x):
    h = 0.001
    deltay = g(x+h) - g(x-h)
    deltax = 2.0*h
    return deltay / deltax

# startverdi -2
# sluttverdi 2
# antall punkter er 50
xverdier = np.linspace(1.0, 2.0, 50)
# helt greit å bruke np.arange istedet for np.linspace
yverdier = g(xverdier)
yverdierderivert = dgdx(xverdier)

plt.figure()
plt.plot(xverdier, yverdier, label="Plott av g(x)")
plt.plot(xverdier, yverdierderivert, label="Plott av dg/dx(x)")
plt.grid()
plt.legend()

#
# Det er kjedelig å lage en ny funksjon hver gang vi
# skal derivere en funksjon. Så vi lager en funksjon
# som deriverer alle funksjoner.
#

# f funksjonen vi skal derivere
# x xverdi(er)
# h stegLengde
def diff(f, x, h):
    return (f(x+h)-f(x-h))/(2.0*h)

def h(x):
    return x**2

def i(x):
    return np.cos(x)

def j(x):
    return x**4

print("dh/dx(2.0)=",diff(h,2.0,0.001))
print("dj/dx(1.0)=",diff(j,1.0,0.01))

